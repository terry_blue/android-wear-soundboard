package uk.co.blueshroom.watchsoundswear;

import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import uk.co.blueshroom.wearsoundboardlibrary.DatabaseManager;

/**
 * Created by Terry on 17/09/2016.
 */
public class SoundsWearableListenerService extends WearableListenerService {

    private final static String TAG = "WSListener(WEAR)";

    public final static String PATH_DB = "/database";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.e(TAG, "onMessageRecieveed: "+messageEvent.getPath());

        String[] compons = messageEvent.getPath().split("database/");
        if(compons.length == 2){
            DatabaseManager dbM = new DatabaseManager(getApplicationContext());
            dbM.saveReplaceFromJSON(compons[1]);
            dbM.close();
        }

        for(int i = 0; i < compons.length; i++)
            Log.e(TAG, "compons["+i+"] = "+compons[i]);

        super.onMessageReceived(messageEvent);
    }
}
