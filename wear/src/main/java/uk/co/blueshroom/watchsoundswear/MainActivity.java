package uk.co.blueshroom.watchsoundswear;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.wearable.view.WearableListView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import uk.co.blueshroom.watchsoundswear.data.SoundEntryRecyclerAdapter;
import uk.co.blueshroom.wearsoundboardlibrary.DatabaseManager;
import uk.co.blueshroom.wearsoundboardlibrary.SoundEntry;


public class MainActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener, DataListener, SoundEntryRecyclerAdapter.OnListSectionRowClickListener {

    final static String TAG = "MainActW";
    Node mNode; // the connected device to send the message to
    GoogleApiClient mGoogleApiClient;

    public static final String SOUND_PLAY_REQUEST_PATH = "/sound_play_request/";
    private WearableListView listView;
    private boolean mResolvingError=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        DatabaseManager dbM = new DatabaseManager(this);

        listView = (WearableListView)findViewById(R.id.main2_listview_soundslist);
        listView.setAdapter(new SoundEntryRecyclerAdapter(this, dbM.getSoundEntriesList(), this));

        //Connect the GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        dbM.close();
    }

    @Override
    public void onResume() {
        super.onResume();


        if (!mResolvingError) {
            mGoogleApiClient.connect();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if(mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Wearable.DataApi.removeListener(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }

    /**
     * Send message to mobile handheld
     */
    private void sendMessage(int id) {

        if (mNode != null && mGoogleApiClient!=null && mGoogleApiClient.isConnected()) {
            Log.e(TAG, "mNode = "+mNode.getDisplayName());
            Wearable.MessageApi.sendMessage(
                    mGoogleApiClient, mNode.getId(), SOUND_PLAY_REQUEST_PATH+Integer.toString(id), null).setResultCallback(

                    new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            Log.e(TAG, "message result = "+sendMessageResult.getStatus().getStatusMessage());
                            if (!sendMessageResult.getStatus().isSuccess()) {
                                Log.e("TAG", "Failed to send message with status code: "
                                        + sendMessageResult.getStatus().getStatusCode());
                            }
                        }
                    }
            );
        }else{
            Log.e(TAG, "some error occurred");
        }
    }
    private void sendTrackTitle(String title) {
        if (mNode != null && mGoogleApiClient!=null && mGoogleApiClient.isConnected()) {
            Log.e(TAG, "mNode = "+mNode.getDisplayName());
            Wearable.MessageApi.sendMessage(
                    mGoogleApiClient, mNode.getId(), SOUND_PLAY_REQUEST_PATH+title, null).setResultCallback(

                    new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            Log.e(TAG, "message result = "+sendMessageResult.getStatus().getStatusMessage());
                            if (!sendMessageResult.getStatus().isSuccess()) {
                                Log.e("TAG", "Failed to send message with status code: "
                                        + sendMessageResult.getStatus().getStatusCode());
                            }
                        }
                    }
            );
        }else{
            Log.e(TAG, "some error occurred");
        }
    }





    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "mGoogleApiClient connected");
        Wearable.DataApi.addListener(mGoogleApiClient, MainActivity.this);
        resolveNode();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed");
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        Log.i(TAG, "onDataChanged Fired");
        DatabaseManager dbM = new DatabaseManager(MainActivity.this);

        for (DataEvent event : dataEventBuffer) {
            DataMap dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();

            if(event.getType() == DataEvent.TYPE_DELETED)
                Log.e(TAG, "onDataChanged() TYPE = DELETED");
            else if(event.getType() == DataEvent.TYPE_CHANGED)
                dbM.saveReplaceFromJSON(dataMap.getString(DatabaseManager.SK_UPDATE_PATH));

        }
        dbM.close();
    }
    

    private void resolveNode() {
        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                for (Node node : nodes.getNodes()) {
                    mNode = node;
                }
            }
        });
    }

    private void putDatabaseToDataMap() {
        Log.e(TAG, "putDatabaseToMap: BEGIN");
        DatabaseManager dbM = new DatabaseManager(MainActivity.this);

        PutDataMapRequest putDataMapReq = PutDataMapRequest.create(DatabaseManager.PATH_UPDATE_PATH);
        //putDataMapReq.getDataMap().remove(DatabaseManager.SK_UPDATE_PATH);
        //putDataMapReq.getDataMap().remove("Time");
        putDataMapReq.getDataMap().putString(DatabaseManager.SK_UPDATE_PATH, dbM.toJSONArray().toString()+"");
        putDataMapReq.getDataMap().putLong("Time", System.currentTimeMillis());
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest().setUrgent();
        PendingResult<DataApi.DataItemResult> pendingResult =
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);

        pendingResult.setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
            @Override
            public void onResult(@NonNull DataApi.DataItemResult dataItemResult) {
                if(dataItemResult.getStatus().isSuccess()) {
                    Log.e(TAG, "putDatabaseToDataMap: pendingResult SUCCESS!");
                    //dataItemResult.getDataItem().
                }
            }
        });
        dbM.close();
    }

    @Override
    public void onAttachmentClick(String title) {
        DatabaseManager dbM = new DatabaseManager(MainActivity.this);
        SoundEntry entry = dbM.getSoundEntryForTitle(title);
        if(entry.ASSIGNED != 0) {
            dbM.assignToTitle(title, 0);
            ((SoundEntryRecyclerAdapter)listView.getAdapter()).setSoundEntryList(dbM.getSoundEntriesList());
        } else {
            int availableAssignment = dbM.getAvailableAssignment();
            if(availableAssignment != 0) { //No assignment available
                dbM.assignToTitle(title, availableAssignment);
                ((SoundEntryRecyclerAdapter)listView.getAdapter()).setSoundEntryList(dbM.getSoundEntriesList());
            }
        }
        putDatabaseToDataMap();
        dbM.close();
    }

    @Override
    public void onPlayClick(String title) {
        sendTrackTitle(title);
    }
}
