package uk.co.blueshroom.watchsoundswear.data;

import android.content.Context;
import android.support.wearable.view.WearableListView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import uk.co.blueshroom.watchsoundswear.R;
import uk.co.blueshroom.wearsoundboardlibrary.DatabaseManager;
import uk.co.blueshroom.wearsoundboardlibrary.SoundEntry;

/**
 * Created by Terry on 17/07/2016.
 */
public class SoundEntryRecyclerAdapter extends WearableListView.Adapter implements View.OnClickListener {

    private final static String TAG = "SE_RECYCLER_ADAPTER";
    private List<SoundEntry> soundEntryList;
    Context context;
    OnListSectionRowClickListener listRowClickListener;

    public SoundEntryRecyclerAdapter(Context cxt, List<SoundEntry> soundEntries, OnListSectionRowClickListener listRowClickListener) {
        soundEntryList = soundEntries;
        context = cxt;
        this.listRowClickListener = listRowClickListener;
    }


    @Override
    public SoundEntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sound_entry_wlvc, parent, false);
        return new SoundEntryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WearableListView.ViewHolder holder, int position) {
        SoundEntryViewHolder holder1 = (SoundEntryViewHolder)holder;
        SoundEntry entry = soundEntryList.get(position);
        holder1.titleTV.setText(entry.TITLE);

        int index = 0;
        int[] circle_ids = new int[] { R.drawable.circle_blue, R.drawable.circle_green, R.drawable.circle_red };
        switch(entry.ASSIGNED) {
            case DatabaseManager.ASSIGN_RED: index++;
            case DatabaseManager.ASSIGN_GREEN: index++;
            case DatabaseManager.ASSIGN_BLUE:
                holder1.assignedIV.setImageResource(circle_ids[index]);
                holder1.assignedIV.setVisibility(View.VISIBLE);
                break;
            case DatabaseManager.ASSIGN_NONE:
                holder1.assignedIV.setVisibility(View.INVISIBLE);
        }

        holder1.playRL.setTag(entry.TITLE);
        holder1.playRL.setOnClickListener(this);
        holder1.assignedRL.setTag(entry.TITLE);
        holder1.assignedRL.setOnClickListener(this);
    }


    @Override
    public int getItemCount() {
        return soundEntryList.size();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.wlvse_rl_play:
                if(view.getTag() != null){
                    if(listRowClickListener != null)
                        listRowClickListener.onPlayClick(view.getTag().toString());
                }

                break;
            case R.id.wlvse_ll_assignment:
                Log.e(TAG, "assignemnt click");
                if(view.getTag() != null){
                    if(listRowClickListener != null)
                        listRowClickListener.onAttachmentClick(view.getTag().toString());
                    else Log.e(TAG, "listRowClickListener = null");
                } else Log.e(TAG, "view.getTag() == null");

                break;
        }
    }

    public void setSoundEntryList(List<SoundEntry> soundEntries) {
        this.soundEntryList = soundEntries;
        notifyDataSetChanged();
    }


    public class SoundEntryViewHolder extends WearableListView.ViewHolder {

        public TextView titleTV;
        public RelativeLayout assignedRL, playRL;
        public LinearLayout titleLL;
        public ImageView assignedIV;

        public SoundEntryViewHolder(View itemView) {
            super(itemView);
            titleTV = (TextView) itemView.findViewById(R.id.wlvse_tv_title);
            assignedRL = (RelativeLayout)itemView.findViewById(R.id.wlvse_ll_assignment);
            playRL = (RelativeLayout)itemView.findViewById(R.id.wlvse_rl_play);
            titleLL = (LinearLayout)itemView.findViewById(R.id.wlvse_ll_title);
            assignedIV = (ImageView)itemView.findViewById(R.id.wlvse_iv_assigned);
        }
    }



    public interface OnListSectionRowClickListener {
        void onAttachmentClick(String title);
        void onPlayClick(String title);
    }





}
