package uk.co.blueshroom.watchsoundswear;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.util.Log;
import android.view.SurfaceHolder;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import uk.co.blueshroom.wearsoundboardlibrary.DatabaseManager;
import uk.co.blueshroom.wearsoundboardlibrary.SoundEntry;

/**
 * Created by pypey on 25/06/16.
 */
public class WatchFaceService extends CanvasWatchFaceService {

    final static int COLOUR_BUTTON_3 = 0xFFF44336;
    final static int COLOUR_BUTTON_2 = 0xFF4CAF50;
    final static int COLOUR_BUTTON_1 = 0xFF2196F3;
    final static int COLOUR_BUTTON_DISABLED = 0x88bbbbbb;

    @Override
    public Engine onCreateEngine() {
        /* provide your watch face implementation */
        return new Engine();
    }

    /* implement service callback methods */
    private class Engine extends CanvasWatchFaceService.Engine implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        final static String TAG = "WF_ENGINE";
        static final int MSG_UPDATE_TIME = 0;
        static final int DATE_TIME_PADDING = 8;
        static final int SEGMENT_PADDING = 16;
        boolean buttonsAvailable = false;

        float BUTTON_CIRCLE_RADIUS;


        Node mNode; // the connected device to send the message to
        GoogleApiClient mGoogleApiClient;
        public static final String PATH_SOUND_PLAY_REQUEST = "/sound_play_request";
        public static final String PATH_MAX_VOLUME = "/request_max_volume";

        Calendar mCalendar;
        // device features
        boolean mLowBitAmbient;
        boolean mBurnInProtection;
        boolean mRegisteredTimeZoneReceiver;

        // graphic objects
        Bitmap mBackgroundBitmap, maxVolBmp;
        Bitmap mBackgroundScaledBitmap;
        Paint mTimePaint, mDatePaint, mButton1Paint, mButton2Paint, mButton3Paint, mGoogleStatusPaint;

        RectF oval1, oval2, oval3;
        Rect btn1R, btn2R, btn3R, connectBtnRect, maxVolRect;
        RectF googleStatusRect;
        float BUTTON_CENTRE;


        final Handler mUpdateTimeHandler = new Handler() {
            @Override
            public void handleMessage(Message message) {
                switch (message.what) {
                    case MSG_UPDATE_TIME:
                        invalidate();
                        if (shouldTimerBeRunning()) {
                            long timeMs = System.currentTimeMillis();
                            long delayMs = 60000
                                    - (timeMs % 60000);
                            mUpdateTimeHandler
                                    .sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
                        }
                        break;
                }
            }
        };

        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);
            setWatchFaceStyle(new WatchFaceStyle.Builder(WatchFaceService.this)
                    .setCardPeekMode(WatchFaceStyle.PEEK_MODE_SHORT)
                    .setAcceptsTapEvents(true)
                    .setBackgroundVisibility(WatchFaceStyle
                            .BACKGROUND_VISIBILITY_INTERRUPTIVE)
                    .setShowSystemUiTime(false)
                    .build());


            mCalendar = Calendar.getInstance();

            // create graphic styles
            mGoogleStatusPaint = new Paint();
            mGoogleStatusPaint.setAntiAlias(true);
            mGoogleStatusPaint.setColor(Color.LTGRAY);
            mGoogleStatusPaint.setTextSize(16);

            mTimePaint = new Paint();
            mTimePaint.setARGB(255, 155, 155, 155);
            mTimePaint.setStrokeWidth(5.0f);
            mTimePaint.setAntiAlias(true);
            mTimePaint.setStrokeCap(Paint.Cap.ROUND);
            mTimePaint.setTextSize(44);

            mDatePaint = new Paint();
            mDatePaint.setARGB(255, 100, 100, 100);
            mDatePaint.setShadowLayer(5, 0, 0, 0x11111111);
            mDatePaint.setStrokeWidth(10.0f);
            mDatePaint.setAntiAlias(true);
            mDatePaint.setStrokeCap(Paint.Cap.ROUND);
            mDatePaint.setTextSize(18);

            mButton1Paint = new Paint(mDatePaint);
            mButton1Paint.setColor(COLOUR_BUTTON_DISABLED);
            mButton2Paint = new Paint(mButton1Paint);
            mButton3Paint = new Paint(mButton2Paint);

            Resources resources = WatchFaceService.this.getResources();
            Drawable backgroundDrawable = resources.getDrawable(R.drawable.background, null);
            Drawable maxVolDrawable = resources.getDrawable(R.drawable.vol_sound, null);

            mBackgroundBitmap = ((BitmapDrawable) backgroundDrawable).getBitmap();
            maxVolBmp = ((BitmapDrawable)maxVolDrawable).getBitmap();



        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
            mBurnInProtection = properties.getBoolean(PROPERTY_BURN_IN_PROTECTION,
                    false);
            invalidate();
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            if (mLowBitAmbient) {
                boolean antiAlias = !inAmbientMode;
                mTimePaint.setAntiAlias(antiAlias);
                mDatePaint.setAntiAlias(antiAlias);
                mButton1Paint.setAntiAlias(antiAlias);
                mButton2Paint.setAntiAlias(antiAlias);
                mButton3Paint.setAntiAlias(antiAlias);

                int alpha = (inAmbientMode) ? 220 : 255;
                int white = (inAmbientMode) ? 128 : 200;

                mTimePaint.setARGB(alpha, white, white, white);
                mDatePaint.setARGB(alpha, white, white, white);
            }

            if(inAmbientMode) {
                if(mGoogleApiClient != null) {
                    mGoogleApiClient.disconnect();
                    mGoogleApiClient = null;
                    buttonsAvailable = false;
                    mButton1Paint.setColor(COLOUR_BUTTON_DISABLED);
                    mButton2Paint.setColor(COLOUR_BUTTON_DISABLED);
                    mButton3Paint.setColor(COLOUR_BUTTON_DISABLED);
                }
            } else {
                //Connect the GoogleApiClient
                mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                        .addApi(Wearable.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();

            }

            invalidate();
            updateTimer();
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            /* draw watch face */
            mCalendar.setTimeInMillis(System.currentTimeMillis());
            String date = mCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault())+" "
                    +mCalendar.get(Calendar.DAY_OF_MONTH)+" "
                    +mCalendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
            String time = mCalendar.get(Calendar.HOUR_OF_DAY)+":"+((mCalendar.get(Calendar.MINUTE) < 10) ? "0" :"")+mCalendar.get(Calendar.MINUTE);

            int timeXPos = (bounds.width() / 2) + 20;
            int timeYPos = (bounds.height() / 3);
            int dateXPos = timeXPos + 10;
            int dateYPos = timeYPos + 20;

            canvas.drawBitmap(mBackgroundScaledBitmap, 0, 0, null);
            canvas.drawText(date, dateXPos, dateYPos, mDatePaint);
            canvas.drawText(time, timeXPos, timeYPos, mTimePaint);

            if(!isInAmbientMode()) {
                canvas.drawRect(oval1, mButton1Paint);
                canvas.drawRect(oval2, mButton2Paint);
                canvas.drawRect(oval3, mButton3Paint);
            }

            if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
                canvas.drawBitmap(maxVolBmp, null, maxVolRect, null);

        }

        @Override
        public void onSurfaceChanged(
                SurfaceHolder holder, int format, int width, int height) {
            if (mBackgroundScaledBitmap == null
                    || mBackgroundScaledBitmap.getWidth() != width
                    || mBackgroundScaledBitmap.getHeight() != height) {
                mBackgroundScaledBitmap = Bitmap.createScaledBitmap(mBackgroundBitmap,
                        width, height, true /* filter */);
            }

            BUTTON_CIRCLE_RADIUS = (float)height / 12f;

            float w3 = ((float)width / 3f);
            float h2 = ((float)height / 2f);
            float h3 = ((float)height / 3f);

            oval1 = new RectF(0, h2 + SEGMENT_PADDING, w3 - (SEGMENT_PADDING/2), height);
            oval2 = new RectF(w3 + (SEGMENT_PADDING/2), h2 + SEGMENT_PADDING, w3 + (w3 - (SEGMENT_PADDING/2)), height);
            oval3 = new RectF((w3*2f) + (SEGMENT_PADDING/2), h2 + SEGMENT_PADDING, width, height);

            btn1R = new Rect((int)oval1.left, (int)oval1.top, (int)oval1.right, (int)oval1.bottom);
            btn2R = new Rect((int)oval2.left, (int)oval2.top, (int)oval2.right, (int)oval2.bottom);
            btn3R = new Rect((int)oval3.left, (int)oval3.top, (int)oval3.right, (int)oval3.bottom);


            maxVolRect = new Rect((int)(w3-BUTTON_CIRCLE_RADIUS), (int)(h3-BUTTON_CIRCLE_RADIUS), (int)(w3+BUTTON_CIRCLE_RADIUS), (int)(h3+BUTTON_CIRCLE_RADIUS));
            connectBtnRect = new Rect(width/8, (int)(h2-24), (width/8)*3, (int)h2);
            googleStatusRect = new RectF(0, h2, width, height);

            BUTTON_CENTRE = ((float)width/16f)*5f;

            super.onSurfaceChanged(holder, format, width, height);
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            /* the watch face became visible or invisible */
            if (visible) {
                registerReceiver();
                mCalendar.setTimeZone(TimeZone.getDefault());
            } else
                unregisterReceiver();

            // Whether the timer should be running depends on whether we're visible and
            // whether we're in ambient mode, so we may need to start or stop the timer
            updateTimer();
        }
        @Override
        public void onTapCommand(
                @TapType int tapType, int x, int y, long eventTime) {
            switch (tapType) {
                case WatchFaceService.TAP_TYPE_TAP:

                    if(mGoogleApiClient != null && !mGoogleApiClient.isConnected() && (btn1R.contains(x,y) || btn2R.contains(x,y) || btn3R.contains(x,y))) {
                        mGoogleApiClient.connect();
                    } else if(mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                        if(btn1R.contains(x,y)) {
                            playAssignedSoundEffect(DatabaseManager.ASSIGN_BLUE);
                        } else if (btn2R.contains(x,y)) {
                            playAssignedSoundEffect(DatabaseManager.ASSIGN_GREEN);
                        } else if (btn3R.contains(x,y)) {
                            playAssignedSoundEffect(DatabaseManager.ASSIGN_RED);
                        } else if(maxVolRect.contains(x,y)) {
                            sendMaxVolumeMessage();
                        }
                        mButton1Paint.setColor(COLOUR_BUTTON_1);
                        mButton2Paint.setColor(COLOUR_BUTTON_2);
                        mButton3Paint.setColor(COLOUR_BUTTON_3);
                        invalidate();
                    }
                    break;

                case WatchFaceService.TAP_TYPE_TOUCH:
                    if(mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                        if (btn1R.contains(x, y)) {
                            mButton1Paint.setARGB(240, 222, 222, 222);
                        } else if (btn2R.contains(x, y)) {
                            mButton2Paint.setARGB(240, 222, 222, 222);
                        } else if (btn3R.contains(x, y)) {
                            mButton3Paint.setARGB(240, 222, 222, 222);
                        }
                        invalidate();
                    }
                    break;

                case WatchFaceService.TAP_TYPE_TOUCH_CANCEL:
                    if(mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                        mButton1Paint.setColor(COLOUR_BUTTON_1);
                        mButton2Paint.setColor(COLOUR_BUTTON_2);
                        mButton3Paint.setColor(COLOUR_BUTTON_3);
                    }
                    invalidate();
                    break;

                default:
                    super.onTapCommand(tapType, x, y, eventTime);
            }
        }



        private void updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        // receiver to update the time zone
        final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            }
        };

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            WatchFaceService.this.registerReceiver(mTimeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            WatchFaceService.this.unregisterReceiver(mTimeZoneReceiver);
        }


        @Override
        public void onConnected(@Nullable Bundle bundle) {
            Log.d(TAG, "onConnected:");
            resolveNode();

            mButton1Paint.setColor(COLOUR_BUTTON_1);
            mButton2Paint.setColor(COLOUR_BUTTON_2);
            mButton3Paint.setColor(COLOUR_BUTTON_3);

            invalidate();
        }




        @Override
        public void onConnectionSuspended(int i) {
            mButton1Paint.setColor(COLOUR_BUTTON_DISABLED);
            mButton2Paint.setColor(COLOUR_BUTTON_DISABLED);
            mButton3Paint.setColor(COLOUR_BUTTON_DISABLED);

            invalidate();
        }

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            mButton1Paint.setColor(COLOUR_BUTTON_DISABLED);
            mButton2Paint.setColor(COLOUR_BUTTON_DISABLED);
            mButton3Paint.setColor(COLOUR_BUTTON_DISABLED);
            //mGoogleStatusPaint.setColor(GOOGLE_STATUS_RED);
            invalidate();
        }

        private void playAssignedSoundEffect(int assigned) {
            DatabaseManager dbM = new DatabaseManager(getApplicationContext());
            SoundEntry soundEntry = dbM.getAssignedSoundEntry(assigned);
            dbM.close();

            if(soundEntry != null)
                sendTrackTitleMessage(soundEntry.TITLE);
            else Log.e(TAG, "soundEntry == NULL");


        }

        private void sendTrackTitleMessage(String trackTitle) {
            if (mNode != null && mGoogleApiClient!=null && mGoogleApiClient.isConnected()) {
                Log.e(TAG, "mNode = "+mNode.getDisplayName());
                Wearable.MessageApi.sendMessage(
                        mGoogleApiClient, mNode.getId(), PATH_SOUND_PLAY_REQUEST +"/"+trackTitle, null).setResultCallback(

                        new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                Log.e(TAG, "message result = "+sendMessageResult.getStatus().getStatusMessage());
                                if (!sendMessageResult.getStatus().isSuccess()) {
                                    Log.e("TAG", "Failed to send message with status code: "
                                            + sendMessageResult.getStatus().getStatusCode());
                                }
                            }
                        }
                );
            } else {
                Log.e(TAG, "some error occurred: (mNode != null): "+(mNode!=null)+", (mGoogleAPIClient != null): "+(mGoogleApiClient != null));
            }
        }
        private void sendMaxVolumeMessage() {
            Log.i(TAG, "sending max vol message");
            if (mNode != null && mGoogleApiClient!=null && mGoogleApiClient.isConnected()) {
                Log.e(TAG, "mNode = "+mNode.getDisplayName());
                Wearable.MessageApi.sendMessage(
                        mGoogleApiClient, mNode.getId(), PATH_MAX_VOLUME , null).setResultCallback(

                        new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                Log.e(TAG, "message result = "+sendMessageResult.getStatus().getStatusMessage());
                                if (!sendMessageResult.getStatus().isSuccess()) {
                                    Log.e("TAG", "Failed to send message with status code: "
                                            + sendMessageResult.getStatus().getStatusCode());
                                }
                            }
                        });
            } else {
                Log.e(TAG, "some error occurred: (mNode != null): "+(mNode!=null)+", (mGoogleAPIClient != null): "+(mGoogleApiClient != null));
            }
        }


        private void resolveNode() {

            Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
                @Override
                public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                    for (Node node : nodes.getNodes()) {
                        mNode = node;
                    }
                }
            });
        }
    }
}
