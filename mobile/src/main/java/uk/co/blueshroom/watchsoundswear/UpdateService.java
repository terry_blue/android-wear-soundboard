package uk.co.blueshroom.watchsoundswear;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import uk.co.blueshroom.wearsoundboardlibrary.DatabaseManager;

/**
 * Created by Terry on 02/07/2016.
 */
public class UpdateService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient googleApiClient = null;

    @Override
    public void onCreate() {
        super.onCreate();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .build();

        googleApiClient.connect();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private class DataTask extends AsyncTask<Node, Void, Void> {

        @Override
        protected Void doInBackground(Node... params) {

            DatabaseManager dbM = new DatabaseManager(UpdateService.this);

            PutDataMapRequest dataMap = PutDataMapRequest.create("/wearablesound/updateservice");
            dataMap.getDataMap().putStringArray("titles", dbM.getAssignedSoundEntryTitles());

            PutDataRequest request = dataMap.asPutDataRequest();
            DataApi.DataItemResult dataItemResult = Wearable.DataApi
                    .putDataItem(googleApiClient, request).await();



            dbM.close();
            return null;
        }
    }
}
