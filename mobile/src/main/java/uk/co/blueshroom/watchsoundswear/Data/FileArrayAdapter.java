package uk.co.blueshroom.watchsoundswear.Data;

/**
 * Created by Terry on 28/11/2015.
 * http://custom-android-dn.blogspot.co.uk/2013/01/create-simple-file-explore-in-android.html
 * 28/11/2015
 */
import java.util.List;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import uk.co.blueshroom.watchsoundswear.R;

public class FileArrayAdapter extends ArrayAdapter<Item>{

    private Context c;
    private int id;
    private List<Item>items;

    public FileArrayAdapter(Context context, int textViewResourceId,
                            List<Item> objects) {
        super(context, textViewResourceId, objects);
        c = context;
        id = textViewResourceId;
        items = objects;
    }
    public Item getItem(int i) {
        return items.get(i);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
            convertView = LayoutInflater.from(c).inflate(R.layout.file_entry, parent, false);


        final Item o = items.get(position);
        if (o != null) {
            TextView t1 = (TextView) convertView.findViewById(R.id.filee_tv_title);
            TextView t2 = (TextView) convertView.findViewById(R.id.filee_tv_data);
            TextView t3 = (TextView) convertView.findViewById(R.id.filee_tv_date);

                       /* Take the ImageView from layout and set the city's image*/
            ImageView imageCity = (ImageView) convertView.findViewById(R.id.filee_iv_icon);

            int imageResource = o.getImage();
            if(imageResource > 0) {
                Drawable image = ContextCompat.getDrawable(c, imageResource);
                imageCity.setImageDrawable(image);
            }

            if(t1!=null)
                t1.setText(o.getName());
            if(t2!=null)
                t2.setText(o.getData());
            if(t3!=null)
                t3.setText(o.getDate());
        }


        return convertView;
    }
}