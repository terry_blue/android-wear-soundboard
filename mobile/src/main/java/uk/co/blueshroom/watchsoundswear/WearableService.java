package uk.co.blueshroom.watchsoundswear;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.IOException;
import java.nio.ByteBuffer;

import uk.co.blueshroom.wearsoundboardlibrary.DatabaseManager;
import uk.co.blueshroom.wearsoundboardlibrary.SoundEntry;


/**
 * Created by Terry on 22/11/2015.
 * Service launched from wearable
 */
public class WearableService extends WearableListenerService {

    private static final String TAG = "WService(MOBILE)";

    public static final String PATH_PLAY_SOUND = "/sound_play_request";
    public static final String PATH_MAX_VOLUME = "/request_max_volume";

    private static final int MAX_SOUND_DURATION_SECS = 30;

    MediaPlayer mp = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate");
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");
        long startTime = System.currentTimeMillis();
        while(mp != null)
            if(startTime + (1000 * MAX_SOUND_DURATION_SECS) < System.currentTimeMillis()) {
                if(mp != null) {
                    mp.release();
                    mp = null;
                }
                break;
            }


        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
        Log.d(TAG, "message Receievfd: "+messageEvent.getPath());

        String fullPath = messageEvent.getPath();
        String[] compons = fullPath.split("/");

        if(compons.length > 2) {
            String path = "/"+compons[1];
            Log.d(TAG, "path = "+path);

            switch(path) {
                case PATH_PLAY_SOUND: {
                    String trackTitle = compons[2];
                    DatabaseManager dbM = new DatabaseManager(getApplicationContext());
                    SoundEntry soundEntry;
                    if(android.text.TextUtils.isDigitsOnly(trackTitle))
                        soundEntry = dbM.getAssignedSoundEntry(Integer.parseInt(trackTitle));
                    else
                        soundEntry = dbM.getSoundEntryForTitle(trackTitle);
                    dbM.close();

                    try {
                        if(mp != null) {
                            mp.release();
                            mp = null;
                        }
                        mp = new MediaPlayer();
                        mp.setDataSource(getApplicationContext(), soundEntry.PATH);
                        mp.prepare();
                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                Log.i(TAG, "mp onCompletionListener");
                                if(mp != null) {
                                    mp.release();
                                    mp = null;
                                }
                            }
                        });
                    } catch (IOException e) {
                        mp.release();
                        mp = null;
                        e.printStackTrace();
                    }

                    if(mp != null)
                        mp.start();

                    break;
                }

            }
        } else if(messageEvent.getPath().equals(PATH_MAX_VOLUME)) {
            Log.d(TAG, "setting MAX_VOLUME");
            AudioManager audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        }
    }
}
