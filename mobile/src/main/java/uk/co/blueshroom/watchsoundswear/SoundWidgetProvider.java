package uk.co.blueshroom.watchsoundswear;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import uk.co.blueshroom.wearsoundboardlibrary.DatabaseManager;
import uk.co.blueshroom.wearsoundboardlibrary.SoundEntry;


/**
 * Created by terrywhite on 24/01/2017.
 */

public class SoundWidgetProvider extends AppWidgetProvider {

    final static String TAG = "SoundWidgetProvider";

    final public static String BTN_CLICK_BLUE = "uk.co.blueshroom.watchsoundswear.soundboardwidget.BUTTON_CLICK_BLUE";
    final public static String BTN_CLICK_GREEN = "uk.co.blueshroom.watchsoundswear.soundboardwidget.BUTTON_CLICK_GREEN";
    final public static String BTN_CLICK_RED = "uk.co.blueshroom.watchsoundswear.soundboardwidget.BUTTON_CLICK_RED";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.e(TAG, "onUpdate");

        ComponentName thisWidget = new ComponentName(context, SoundWidgetProvider.class);

        //for (int appWidgetID : appWidgetIds) {
            Log.e(TAG, "new app widgetID");
            RemoteViews remoteViews;
            ComponentName watchWidget;

            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_basic);
            //thisWidget = new ComponentName(context, SoundWidgetProvider.class);

            remoteViews.setOnClickPendingIntent(R.id.widget_btn_blue, getPendingSelfIntent(context, BTN_CLICK_BLUE));
            remoteViews.setOnClickPendingIntent(R.id.widget_btn_green, getPendingSelfIntent(context, BTN_CLICK_GREEN));
            remoteViews.setOnClickPendingIntent(R.id.widget_btn_red, getPendingSelfIntent(context, BTN_CLICK_RED));

            appWidgetManager.updateAppWidget(thisWidget, remoteViews);
        //}
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.e(TAG, "onRecieve");
        DatabaseManager dbM = new DatabaseManager(context);
        SoundEntry soundEntry;
        if(intent.getAction() != null && !intent.getAction().isEmpty()) {
            Log.e(TAG, "action = " + intent.getAction());

            switch (intent.getAction()) {
                case BTN_CLICK_BLUE:
                    soundEntry = dbM.getAssignedSoundEntry(DatabaseManager.ASSIGN_BLUE);
                    Log.e(TAG, "BLUE clicked");
                    break;
                case BTN_CLICK_GREEN:
                    soundEntry = dbM.getAssignedSoundEntry(DatabaseManager.ASSIGN_GREEN);
                    Log.e(TAG, "GREEN clicked");
                    break;
                case BTN_CLICK_RED:
                    soundEntry = dbM.getAssignedSoundEntry(DatabaseManager.ASSIGN_RED);
                    Log.e(TAG, "RED clicked");
                    break;
            }
        } else {
            Log.e(TAG, "onReceive intent == null");
        }
        dbM.close();
    }





    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, SoundWidgetProvider.class);
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

}
