package uk.co.blueshroom.watchsoundswear;

import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import uk.co.blueshroom.wearsoundboardlibrary.DatabaseManager;
import uk.co.blueshroom.wearsoundboardlibrary.SoundEntryCursorAdapter;
import uk.co.blueshroom.wearsoundboardlibrary.SoundEntry;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, MediaPlayer.OnCompletionListener, DataApi.DataListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    final public static String TAG = "MobileMain";
    final public static String PATH_DATABASE = "/database";

    MediaPlayer mp = null;

    public final static int REQUEST_NEW_FILEPATH = 202;
    GoogleApiClient mGoogleApiClient;
    Node mNode;

    final static String SPK_FIRST_TIME = "uk.co.blueshroom.watchsoundswear.MainActivity.SPK_FIRST_TIME";
    final private static String CONFIGURE_ACTION="android.appwidget.action.APPWIDGET_CONFIGURE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Connect the GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        DatabaseManager dbM = new DatabaseManager(this);

        Log.e("COUNT", "char count = "+dbM.toJSONArray().toString().length());

        ListView lv = (ListView)findViewById(R.id.main_lv_content);
        lv.setAdapter(new SoundEntryCursorAdapter(this, dbM.getSoundEntriesCursor(), true));
        lv.setOnItemClickListener(this);
        registerForContextMenu(lv);


        findViewById(R.id.main_iv_red).setAlpha((dbM.getAssignedSoundEntry(1) == null) ? 1f : 0.2f);
        findViewById(R.id.main_iv_green).setAlpha((dbM.getAssignedSoundEntry(2) == null) ? 1f : 0.2f);
        findViewById(R.id.main_iv_blue).setAlpha((dbM.getAssignedSoundEntry(3) == null) ? 1f : 0.2f);


        String[] titles = dbM.getAssignedSoundEntryTitles();
        for(String title : titles) {
            Log.e("MAIN", "title = "+title);
        }



        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        if(sp.getBoolean(SPK_FIRST_TIME, true)) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean(SPK_FIRST_TIME, false);
            editor.apply();

            if (dbM.getEntryCount() == 0) {
                List<SoundEntry> theList = getEntriesForStoring();
                for (SoundEntry entry : theList)
                    dbM.addSoundEntry(entry);
            }

            Log.e(TAG, "onCreate toDatabaseMap()");
            dbM.close();
            putDatabaseToDataMap();
        } else {
            dbM.close();
        }




    }

    @Override
    public void onResume() {
        super.onResume();

        mGoogleApiClient.connect();
    }

    @Override
    public void onBackPressed() {
        if (CONFIGURE_ACTION.equals(getIntent().getAction())) {
            Intent intent = getIntent();
            Bundle extras = intent.getExtras();

            if (extras != null) {
                int id = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                        AppWidgetManager.INVALID_APPWIDGET_ID);
                AppWidgetManager mgr = AppWidgetManager.getInstance(this);
                RemoteViews views = new RemoteViews(getPackageName(),
                        R.layout.widget_basic);

                mgr.updateAppWidget(id, views);

                Intent result = new Intent();

                result.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
                setResult(RESULT_OK, result);
                sendBroadcast(new Intent(this, SoundWidgetProvider.class));
            }
        }

        super.onBackPressed();
    }


    private void refreshData() {
        DatabaseManager dbM = new DatabaseManager(this);

        ListView lv = (ListView)findViewById(R.id.main_lv_content);
        int index = lv.getFirstVisiblePosition();
        View v = lv.getChildAt(0);
        int top = (v == null) ? 0 : v.getTop();

        ((CursorAdapter)lv.getAdapter()).swapCursor(dbM.getSoundEntriesCursor());
        lv.setSelectionFromTop(index, top);

        findViewById(R.id.main_iv_red).setAlpha((dbM.getAssignedSoundEntry(1) == null) ? 1f : 0.2f);
        findViewById(R.id.main_iv_green).setAlpha((dbM.getAssignedSoundEntry(2) == null) ? 1f : 0.2f);
        findViewById(R.id.main_iv_blue).setAlpha((dbM.getAssignedSoundEntry(3) == null) ? 1f : 0.2f);

        dbM.close();
    }

    private List<SoundEntry> getEntriesForStoring() {
        ArrayList<SoundEntry> entries = new ArrayList<>();
        Field[] fields=R.raw.class.getFields();
        for (Field field : fields) {
            if(!field.getName().equals("gtm_analytics")) {
                try {
                    Log.e(TAG, "fieldName = "+field.getName()+", resID = "+field.getInt(null)+", rawID(boom) = "+R.raw.boom);
                    entries.add(new SoundEntry(field.getName()
                            , Uri.parse("android.resource://" + getPackageName() + "/" + field.getInt(null))
                            , 0));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return entries;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(mp != null) {
            mp.release();
            mp = null;
        }

        DatabaseManager dbM = new DatabaseManager(MainActivity.this);
        Cursor c = dbM.getSoundEntriesCursor();
        c.moveToPosition(position);
        mp = MediaPlayer.create(MainActivity.this, Uri.parse(c.getString(c.getColumnIndex(DatabaseManager.C_PATH))));
        mp.start();
        c.close();
        dbM.close();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        this.mp.release();
        this.mp = null;
    }


    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_start_filex: {
                Intent intent = new Intent(this, FileExplorerActivity.class);
                startActivityForResult(intent, REQUEST_NEW_FILEPATH);
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        // See which child activity is calling us back.
        if (requestCode == REQUEST_NEW_FILEPATH){
            if (resultCode == RESULT_OK) {
                final String filename = data.getStringExtra("GetFileName");
                final String dir = data.getStringExtra("GetPath");
                DatabaseManager dbM = new DatabaseManager(this);
                dbM.addSoundEntry(new SoundEntry(filename
                                                , Uri.parse(dir + "/" + filename)
                                                , 0));

                dbM.close();
                refreshData();

                Log.e(TAG, "onActivityResult toDatabaseMap()");
                putDatabaseToDataMap();
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.main_lv_content) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.sound_entry, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuinfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int position = menuinfo.position; //position in the adapter
        DatabaseManager dbM = new DatabaseManager(MainActivity.this);
        Cursor c = dbM.getSoundEntriesCursor();
        c.moveToPosition(position);

        switch(item.getItemId()) {
            case R.id.sem_assign:
                if(c.getInt(c.getColumnIndex(DatabaseManager.C_ASSIGNED)) == 0)
                    dbM.setAssigned(c.getString(c.getColumnIndex(DatabaseManager.C_ID)), dbM.getAvailableAssignment());
                else
                    dbM.setAssigned(c.getString(c.getColumnIndex(DatabaseManager.C_ID)), 0);
                break;
            case R.id.sem_rename:
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.inflate(R.layout.rename_prompt, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        this);

                // set prompts.xml to alertdialog buil
                alertDialogBuilder.setView(promptsView);
                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.rename_et_new);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        DatabaseManager dbM = new DatabaseManager(MainActivity.this);
                                        Cursor c = dbM.getSoundEntriesCursor();
                                        // get user input and set it to result
                                        // edit text
                                        dbM.setTitle(c.getString(c.getColumnIndex(DatabaseManager.C_ID)), userInput.getText().toString());

                                        c.close();
                                        dbM.close();
                                        refreshData();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                break;
            default:
                return super.onContextItemSelected(item);
        }

        c.close();
        dbM.close();
        refreshData();
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Wearable.DataApi.addListener(mGoogleApiClient, MainActivity.this);
        resolveNode();
        //putDatabaseToDataMap();


    }


    private void putDatabaseToDataMap() {
        Log.e(TAG, "putDatabaseToMap: BEGIN");
        DatabaseManager dbM = new DatabaseManager(MainActivity.this);

        PutDataMapRequest putDataMapReq = PutDataMapRequest.create(DatabaseManager.PATH_UPDATE_PATH);
        //putDataMapReq.getDataMap().remove(DatabaseManager.SK_UPDATE_PATH);
        //putDataMapReq.getDataMap().remove("Time");
        putDataMapReq.getDataMap().putString(DatabaseManager.SK_UPDATE_PATH, dbM.toJSONArray().toString()+"");
        putDataMapReq.getDataMap().putLong("Time", System.currentTimeMillis());
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest().setUrgent();
        PendingResult<DataApi.DataItemResult> pendingResult =
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);

        pendingResult.setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
            @Override
            public void onResult(@NonNull DataApi.DataItemResult dataItemResult) {
                if(dataItemResult.getStatus().isSuccess()) {
                    Log.e(TAG, "putDatabaseToDataMap: pendingResult SUCCESS!");
                    //dataItemResult.getDataItem().
                }
            }
        });
        dbM.close();
    }

    private void sendDatabaseAsMessage() {
            if (mNode != null && mGoogleApiClient!=null && mGoogleApiClient.isConnected()) {
                Log.e(TAG, "mNode = "+mNode.getDisplayName());
                DatabaseManager dbM = new DatabaseManager(this);
                Wearable.MessageApi.sendMessage(
                        mGoogleApiClient, mNode.getId(), PATH_DATABASE + "/" + dbM.toJSONArray().toString(), null).setResultCallback (

                        new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                Log.e(TAG, "message result = "+sendMessageResult.getStatus().getStatusMessage());
                                if (!sendMessageResult.getStatus().isSuccess()) {
                                    Log.e("TAG", "Failed to send message with status code: "
                                            + sendMessageResult.getStatus().getStatusCode());
                                }
                            }
                        }
                );
            } else {
                Log.e(TAG, "some error occurred: (mNode != null): "+(mNode!=null)+", (mGoogleAPIClient != null): "+(mGoogleApiClient != null));
            }
    }

    private void resolveNode() {
        Log.w(TAG, "resolving nodes");
        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                for (Node node : nodes.getNodes()) {
                    mNode = node;
                    Log.e(TAG, "node resoleved");
                }

                sendDatabaseAsMessage();

            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        Log.e(TAG, "onDataChanged Fired");
        DatabaseManager dbM = new DatabaseManager(MainActivity.this);

        for (DataEvent event : dataEventBuffer) {
            DataMap dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();

            if(event.getType() == DataEvent.TYPE_DELETED)
                Log.e(TAG, "onDataChanged() TYPE = DELETED");
            else if(event.getType() == DataEvent.TYPE_CHANGED)
                dbM.saveReplaceFromJSON(dataMap.getString(DatabaseManager.SK_UPDATE_PATH));

        }
        dbM.close();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
