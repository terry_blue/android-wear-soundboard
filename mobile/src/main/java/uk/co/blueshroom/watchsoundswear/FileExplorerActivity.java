package uk.co.blueshroom.watchsoundswear;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.text.DateFormat;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import uk.co.blueshroom.watchsoundswear.Data.FileArrayAdapter;
import uk.co.blueshroom.watchsoundswear.Data.Item;

public class FileExplorerActivity extends AppCompatActivity {

    private static final String TAG = "Explorer";
    private static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 33;
    private File currentDir;
    private FileArrayAdapter adapter;
    String rootFilepath;

    String[] VALIDS = new String[] { "ogg", "mp3", "3gp", "m4a", "mp4", "wav" };
    List<String> validList = Arrays.asList(VALIDS);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_explorer);
        Log.e(TAG, "start oon onCreate");
        Arrays.sort(VALIDS);
        validList = Arrays.asList(VALIDS);

        if (Build.VERSION.SDK_INT >= 23) {
            checkPermission();
        } else {
            currentDir = Environment.getExternalStorageDirectory();
            rootFilepath = currentDir.toString();
            fill(currentDir);
        }
        Log.e(TAG, "end of onCreate");
    }

    String parent;
    private void fill(File f) {
        File[] dirs = f.listFiles();
        this.setTitle(f.getName());
        List<Item> dir = new ArrayList<>();
        List<Item> fls = new ArrayList<>();
        try {
            for (File ff : dirs) {
                Date lastModDate = new Date(ff.lastModified());
                DateFormat formater = DateFormat.getDateTimeInstance();
                String date_modify = formater.format(lastModDate);
                if (ff.isDirectory()) {
                    int validCount = countValids(ff);
                    String validsString = Integer.toString(validCount)+" valid items";
                    dir.add(new Item(ff.getName(), validsString, "", ff.getAbsolutePath(), R.drawable.folderpng));
                } else {
                    String fT = ff.getAbsolutePath().substring(ff.getAbsolutePath().lastIndexOf('.') + 1);

                    if(validList.contains(fT))
                        fls.add(new Item(ff.getName(), ff.length() + " Byte", date_modify, ff.getAbsolutePath(), R.drawable.filepng));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "fill Exception: "+e.toString());
        }


        Log.i(TAG, f.getName() + " dir size = "+dir.size()+ " fls size = "+fls.size());
        if(dir.size() == 0 && fls.size() == 0) {
            fls.add(new Item("Only valid file types are shown", Arrays.toString(VALIDS), "", "", R.drawable.icn_x));
        }

        Collections.sort(dir);
        Collections.sort(fls);
        dir.addAll(fls);
        if (!currentDir.toString().equals(rootFilepath)) {
            dir.add(0, new Item("..", "Parent Directory", "", f.getParent(), R.drawable.folderuppng));
            parent = f.getParent();
        }
        adapter = new FileArrayAdapter(FileExplorerActivity.this, R.layout.file_entry, dir);

        ListView lv = (ListView)findViewById(R.id.explore_lv_content);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item o = adapter.getItem(position);
                if (o.getImage() == R.drawable.folderpng || o.getImage() == R.drawable.folderuppng) {
                    currentDir = new File(o.getPath());
                    fill(currentDir);
                } else
                    onFileClick(o);
            }
        });
    }

    private void onFileClick(Item o) {
        //Toast.makeText(this, "Folder Clicked: "+ currentDir, Toast.LENGTH_SHORT).show();
        if(o.getImage() != R.drawable.icn_x) {
            Intent intent = new Intent();
            intent.putExtra("GetPath", currentDir.toString());
            intent.putExtra("GetFileName", o.getName());
            setResult(RESULT_OK, intent);
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        if(currentDir.toString().equals(rootFilepath))
            super.onBackPressed();
        else {
            currentDir = new File(parent);
            fill(currentDir);
        }
    }




    private int countValids(File file) {
        if(file.isDirectory()) {
            int count = 0;
            for(File f : file.listFiles())
                count += countValids(f);
            return count;
        }
        return (validList.contains(file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf('.') + 1))) ? 1 : 0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d(TAG, "onRequestPermissions result");
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // task you need to do.

                    Log.d(TAG, "Permissions GRANTED");

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "PERMISSION DENIED");

                }
                currentDir = Environment.getExternalStorageDirectory();
                rootFilepath = currentDir.toString();
                fill(currentDir);

                Log.d(TAG, "stuff should be done");
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



    private void checkPermission() {
        Log.d(TAG, "checking permissions");
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Should we show an explanation?");
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Log.d(TAG, "Show an explanation to the user *asynchronously*");
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                new AlertDialog.Builder(this)
                        .setTitle("Permission Required")
                        .setMessage("This permission is required to allow you to browse through your files")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(FileExplorerActivity.this,
                                        new String[]{ android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                        MY_PERMISSIONS_REQUEST_READ_STORAGE);
                            }
                        }).create().show();


            } else {

                // No explanation needed, we can request the permission.

                Log.d(TAG, "No explanation needed, we can request the permission.");
                ActivityCompat.requestPermissions(this,
                        new String[]{ android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_STORAGE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            currentDir = Environment.getExternalStorageDirectory();
            rootFilepath = currentDir.toString();
            fill(currentDir);
        }
    }
}
