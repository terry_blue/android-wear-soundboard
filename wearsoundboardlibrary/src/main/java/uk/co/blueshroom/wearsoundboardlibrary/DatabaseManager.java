package uk.co.blueshroom.wearsoundboardlibrary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Terry on 28/11/2015.
 * File used to access the database containing all sound entries available to the app
 */
public class DatabaseManager extends SQLiteOpenHelper {

    private final static String TAG = "DatabaseManager";
    public final static String DB_NAME = "SoundEntryDB";
    public final static int DB_VERSION = 1;
    protected SQLiteDatabase db;

    private final static int MAX_ASSIGNED = 3;

    public final static String SK_UPDATE_PATH = "uk.co.blueshroom.wearsoundboardlibrary.DatabaseManager.SK_UPDATE_PATH";
    public final static String PATH_UPDATE_PATH = "/androidwearsounds/update";

    public final static String T_SOUND_ENTRIES = "SoundEntries";
    public final static String C_ID = "_id";
    public final static String C_TITLE = "Title";
    public final static String C_PATH = "Path";
    public final static String C_ASSIGNED = "Assigned";
    public final static String[] COLUMNS = new String[] { C_ID, C_TITLE, C_PATH, C_ASSIGNED };

    public final static int ASSIGN_BLUE = 3;
    public final static int ASSIGN_GREEN = 2;
    public final static int ASSIGN_RED = 1;
    public final static int ASSIGN_NONE = 0;

    public DatabaseManager(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG, "Database created");
        String createString = "CREATE TABLE "+T_SOUND_ENTRIES+" ("
                +  C_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + C_TITLE + " TEXT UNIQUE, "
                + C_PATH + " TEXT, "
                + C_ASSIGNED + " INTEGER"
                +")";
        db.execSQL(createString);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public int getEntryCount() {
        Cursor c = db.query(T_SOUND_ENTRIES, new String[] { C_ID }, null, null, null, null, null);
        int count = c.getCount();
        c.close();
        return count;
    }



    public void addSoundEntry(SoundEntry entry) {
        ContentValues values = new ContentValues();
        values.put(C_TITLE, entry.TITLE);
        values.put(C_PATH, entry.PATH.toString());

        //Work out if already assigned before insert
        Cursor c = db.query(T_SOUND_ENTRIES, new String[] { C_TITLE, C_ASSIGNED }, C_TITLE+"=?", new String[] { entry.TITLE }, null, null, null);
        c.moveToFirst();
        if(c.getCount() > 0) {
            values.put(C_ASSIGNED, c.getInt(c.getColumnIndex(C_ASSIGNED)));
        } else {
            values.put(C_ASSIGNED, entry.ASSIGNED);
        }
        c.close();


        db.insertWithOnConflict(T_SOUND_ENTRIES, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }


    public SoundEntry getAssignedSoundEntry(int assignment) {
        Cursor c = db.query(T_SOUND_ENTRIES, COLUMNS, C_ASSIGNED+"=?", new String[] { Integer.toString(assignment
        )}, null, null, null);
        c.moveToFirst();

        if(!c.isAfterLast()) {
            String title = c.getString(c.getColumnIndex(C_TITLE));
            Uri path = Uri.parse(c.getString(c.getColumnIndex(C_PATH)));
            int assigned = c.getInt(c.getColumnIndex(C_ASSIGNED));
            c.close();
            return new SoundEntry(title, path, assigned);
        }
        c.close();
        return null;
    }
    public SoundEntry getSoundEntryForTitle(String title) {
        Cursor c = db.query(T_SOUND_ENTRIES, COLUMNS, C_TITLE+"=?", new String[] { title }, null, null, null);
        c.moveToFirst();
        if(!c.isAfterLast()) {
            Uri path = Uri.parse(c.getString(c.getColumnIndex(C_PATH)));
            int assigned = c.getInt(c.getColumnIndex(C_ASSIGNED));
            c.close();
            return new SoundEntry(title, path, assigned);
        }
        c.close();
        return null;
    }


    public String[] getAssignedSoundEntryTitles() {
        String[] ret = new String[4];
        Cursor c = db.query(T_SOUND_ENTRIES, new String[]{C_TITLE}, C_ASSIGNED+" >= 1 AND "+C_ASSIGNED+" <= 4", new String[]{}, null, null, C_ASSIGNED);
        c.moveToFirst();

        int index = 0;
        while(!c.isAfterLast() && index < 5) {
            ret[index] = c.getString(c.getColumnIndex(C_TITLE));
            index++;
            c.moveToNext();
        }

        c.close();
        return ret;
    }




    public Cursor getSoundEntriesCursor() {
        Cursor c = db.query(T_SOUND_ENTRIES, COLUMNS, null, null, null, null, C_ASSIGNED+" DESC, "+C_TITLE);
        c.moveToFirst();
        return c;
    }

    public List<SoundEntry> getSoundEntriesList() {
        ArrayList<SoundEntry> entries = new ArrayList<>();
        Cursor c = getSoundEntriesCursor();
        while(!c.isAfterLast()) {
            int assigned = c.getInt(c.getColumnIndex(C_ASSIGNED));
            String title = c.getString(c.getColumnIndex(C_TITLE));
            Uri path = Uri.parse(c.getString(c.getColumnIndex(C_PATH)));
            entries.add(new SoundEntry(title, path, assigned));
            c.moveToNext();
        }

        return entries;
    }


    public int getAvailableAssignment() {
        for(int i = 1; i <= MAX_ASSIGNED; i++) {
            Cursor c = db.query(T_SOUND_ENTRIES, new String[] { C_ID }, C_ASSIGNED+"=?", new String[] { Integer.toString(i) }, null, null, null);
            if(c.getCount() == 0) {
                c.close();
                Log.e(TAG, "getAvailableAssignment returning: "+i);
                return i;
            }
            c.close();
        }
        Log.e(TAG, "getAvailableAssignment: returning 0");
        return 0;
    }

    public void assignToTitle(String title, int assign) {
        ContentValues values = new ContentValues();
        values.put(C_ASSIGNED, assign);
        db.updateWithOnConflict(T_SOUND_ENTRIES, values, C_TITLE+"=?", new String[]{ title }, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void setAssigned(String id, int assigned) {
        ContentValues values = new ContentValues();
        values.put(C_ASSIGNED, assigned);
        db.updateWithOnConflict(T_SOUND_ENTRIES, values, C_ID+"=?", new String[] { id }, SQLiteDatabase.CONFLICT_REPLACE );
    }
    public void setTitle(String id, String newTitle) {
        ContentValues values = new ContentValues();
        values.put(C_TITLE, newTitle);
        db.updateWithOnConflict(T_SOUND_ENTRIES, values, C_ID+"=?", new String[] { id }, SQLiteDatabase.CONFLICT_REPLACE);
    }



    public void saveReplaceFromJSON(String dataJSON) {
        db.delete(T_SOUND_ENTRIES, null, null);

        JSONArray dbData = new JSONArray();

        try {
            dbData = new JSONArray(dataJSON);
        } catch(JSONException e){ Log.e(TAG, "error creating save JSON array: "+e.toString()); }

        int recCount = dbData.length();
        for(int i = 0; i < recCount; i++) {
            try {
                JSONObject obj = dbData.getJSONObject(i);
                ContentValues vals = new ContentValues();

                vals.put(C_ID, obj.getInt(C_ID));
                vals.put(C_ASSIGNED, obj.getInt(C_ASSIGNED));
                vals.put(C_TITLE, obj.getString(C_TITLE));
                vals.put(C_PATH, obj.getString(C_PATH));

                db.insertWithOnConflict(T_SOUND_ENTRIES, null, vals, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (JSONException e) { Log.e(TAG, "Error saving from JSON: "+e.toString()); }
        }
    }



    public JSONArray toJSONArray() {
        JSONArray retArray = new JSONArray();
        Cursor c = getSoundEntriesCursor();
        while(!c.isAfterLast()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put(C_ID, c.getInt(c.getColumnIndex(C_ID)))
                        .accumulate(C_TITLE, c.getString(c.getColumnIndex(C_TITLE)))
                        .accumulate(C_PATH, c.getString(c.getColumnIndex(C_PATH)))
                        .accumulate(C_ASSIGNED, c.getInt(c.getColumnIndex(C_ASSIGNED)));
            } catch (JSONException e) { Log.e(TAG, "toJSONArray exception: "+e.toString()); }

            retArray.put(obj);
            c.moveToNext();
        }

        return retArray;
    }
}
