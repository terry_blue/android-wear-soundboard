package uk.co.blueshroom.wearsoundboardlibrary;

import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Terry on 25/11/2015.
 */
public class SoundEntry {

    final public  Uri PATH;
    final public String TITLE;
    final public int ASSIGNED;

    public SoundEntry(String title, Uri path, int assigned) {
        this.TITLE = title;
        this.PATH = path;
        this.ASSIGNED = assigned;
    }

    @Override
    public String toString() {
        return "TITLE="+ TITLE +", PATH="+ PATH +", ASSIGNED="+ ASSIGNED;
    }



    public JSONObject toJSON() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.accumulate("PATH", PATH)
                    .accumulate("TITLE", TITLE)
                    .accumulate("ASSIGNED", ASSIGNED);
        } catch(JSONException e) { Log.e("SoundEntry", "Error creating json object: "+e.toString()); }

        return jObj;
    }
}
