package uk.co.blueshroom.wearsoundboardlibrary;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Terry on 28/11/2015.
 * SoundEntryCursor
 */
public class SoundEntryCursorAdapter extends CursorAdapter {

    public SoundEntryCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.sound_entry, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        //TextView assignedTV = (TextView)view.findViewById(R.id.entry_tv_assigned);
        ImageView assignedIV = (ImageView)view.findViewById(R.id.entry_iv_assigned);
        TextView titleTV = (TextView)view.findViewById(R.id.entry_tv_title);


        //assignedTV.setText(cursor.getString(cursor.getColumnIndex(DatabaseManager.C_ASSIGNED)));
        titleTV.setText(cursor.getString(cursor.getColumnIndex(DatabaseManager.C_TITLE)));

        int assigned = cursor.getInt(cursor.getColumnIndex(DatabaseManager.C_ASSIGNED));
        assignedIV.setVisibility((assigned == 0) ? View.INVISIBLE : View.VISIBLE);
        switch (assigned) {
            case 1:
                assignedIV.setImageResource(R.drawable.round_rect_red);
                break;
            case 2:
                assignedIV.setImageResource(R.drawable.round_rect_green);
                break;
            case 3:
                assignedIV.setImageResource(R.drawable.round_rect_blue);
                break;
        }
    }
}
